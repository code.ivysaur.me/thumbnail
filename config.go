package thumbnail

type OutputFormat uint8
type AspectFormat uint8
type ScaleFormat uint8

const (
	Png      OutputFormat = 1
	PngCrush OutputFormat = 2
	Jpeg     OutputFormat = 3
	Bmp      OutputFormat = 4

	FitOutside AspectFormat = 80 // Pad out with black bars to dimensions
	FitInside  AspectFormat = 82 // Crop to dimensions
	Stretch    AspectFormat = 83 // Doesn't preserve aspect ratio

	NearestNeighbour ScaleFormat = 120
	BilinearFast     ScaleFormat = 121
	BilinearAccurate ScaleFormat = 122
	Bicubic          ScaleFormat = 123
)

type Config struct {
	Width  int
	Height int
	Output OutputFormat
	Aspect AspectFormat
	Scale  ScaleFormat
}

var DefaultConfig = Config{
	Width:  128,
	Height: 128,
	Output: Jpeg,
	Aspect: FitOutside,
	Scale:  Bicubic,
}
