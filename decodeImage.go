package thumbnail

import (
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"os"

	"golang.org/x/image/bmp"
	"golang.org/x/image/webp"
)

type imageDecoder func(io.Reader) (image.Image, error)

var imageDecoders map[string]imageDecoder = map[string]imageDecoder{
	`image/jpeg`: jpeg.Decode,
	`image/png`:  png.Decode,
	`image/gif`:  gif.Decode,
	`image/webp`: webp.Decode,
	`image/bmp`:  bmp.Decode,
}

func (this *DirectThumbnailer) imageSnapshot(absPath, mimeType string) (image.Image, error) {

	// Load file from path

	fh, err := os.OpenFile(absPath, os.O_RDONLY, 0400)
	if err != nil {
		return nil, err
	}
	defer fh.Close()

	// Decode image

	fn, ok := imageDecoders[mimeType]
	if !ok {
		return nil, ErrUnsupportedFiletype
	}

	return fn(fh)
}
