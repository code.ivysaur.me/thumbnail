//+build !cgo

package thumbnail

import (
	"errors"
	"image"
)

func crushFast(img image.Image) ([]byte, error) {
	return nil, errors.New("Pngquant not compiled in")
}
