package main

import (
	"flag"
	"fmt"
	"os"

	"code.ivysaur.me/thumbnail"
)

func main() {
	width := flag.Int("Width", 100, "Width")
	height := flag.Int("Height", 100, "Height")
	flag.Parse()

	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Expected one filename")
		os.Exit(1)
	}

	fname := flag.Arg(0)

	th := thumbnail.NewThumbnailer(*width, *height)
	data, err := th.RenderFile(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	fmt.Print(string(data))
	os.Exit(0)

}
