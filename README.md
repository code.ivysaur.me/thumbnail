# thumbnail

A thumbnailing library for Go.

- Supports jpeg / png / gif / bmp / webp files (internally)
- Supports video files (requires `ffmpeg` in `$PATH`)
- Optional LRU cache of recent thumbnails for performance
- Sampling algorithms: Nearest-neighbour, Bilinear (fast), Bilinear (accurate), Bicubic (Catmull-Rom)
- Scaling algorithms: Fit inside, Fit outside, Stretch
- Output formats: JPG, PNG, Quantised/lossy PNG (via `go-imagequant` if CGO is available), BMP

A standalone binary `mkthumb` is provided as a sample utility.

This package can be installed via go get: `go get code.ivysaur.me/thumbnail`

## Changelog

2020-07-25 1.0.2
- Automatically build pngquant support if CGO is available
- Remove vendor directory, rely more heavily on Go Modules
- Update module dependencies

2018-12-31 1.0.1
- Convert to Go Modules
- Update vendored dependencies

2018-06-09 1.0.0
- Feature: Major speed improvement
- Feature: Support FitInside, FitOutside, and Stretch stretching modes
- Feature: Support Bilnear (Fast), Bilnear (Accurate), and Bicubic (Catmull-Rom) scaling algorithms
- Feature: Support PNG output without imagequant; support BMP output
- Feature: Support Webp and BMP input files
- Fix wrong dimension output for video input files
- Fix jagged output of bilinear resizer
- [⬇️ thumbnail-1.0.0-src.tar.xz](https://git.ivysaur.me/attachments/aa9c5b40-3faa-45c3-a45e-10004d03481c) *(4.78 KiB)*

2018-06-04 0.2.1
- Add `disableimagecrush` build tag option to exclude cgo library dependency
- [⬇️ thumbnail-0.2.1-src.zip](https://git.ivysaur.me/attachments/08c12811-fc7b-44b0-93fd-161091632e9e) *(24.09 KiB)*

2017-11-18 0.2.0
- Initial standalone release
- Feature: Decode input with specified mime type
- Feature: Allow passing zero as thumbnail cache size
- [⬇️ thumbnail-0.2.0-src.zip](https://git.ivysaur.me/attachments/c2405a1a-50c3-4059-9b49-a77bc2ee311f) *(23.37 KiB)*

2017-01-03 0.1.0
- Version of `thumbnail` vendored with `webdir` 1.0 (previously tagged as `release-1.0`)
