module code.ivysaur.me/thumbnail

require (
	code.ivysaur.me/imagequant/v2 v2.12.6
	github.com/hashicorp/golang-lru v0.5.4
	golang.org/x/image v0.0.0-20200618115811-c13761719519
)

go 1.13
