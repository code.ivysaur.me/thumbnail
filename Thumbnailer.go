package thumbnail

import (
	"errors"
	"strings"
)

var (
	ErrInvalidOption       error = errors.New("Invalid format parameter")
	ErrUnsupportedFiletype error = errors.New("Unsupported filetype")
)

type Thumbnailer interface {
	RenderFile(absPath string) ([]byte, error)
	RenderFileAs(absPath, mimeType string) ([]byte, error)
}

func FiletypeSupported(ext string) bool {
	switch strings.ToLower(ext) {
	case
		".jpg", ".jpeg", ".png", ".gif",
		".avi", ".mkv", ".mp4", ".ogm", ".wmv", ".flv", ".rm", ".rmvb",
		".bmp", ".webp":
		return true
	default:
		return false
	}
}
